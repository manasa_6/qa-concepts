package realtimequestions;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;

public class Actions_class {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
  WebDriver driver= new FirefoxDriver();
  System.setProperty("webdriver.gecko.driver", "C:\\drivers\\geckodriver.exe");
  driver.get("http://www.amazon.com");
  //here abc is having both the driver and actions capabilities 
  Actions abc= new Actions(driver);
  WebElement Element=driver.findElement(By.xpath(".//*[@id='nav-link-accountList']"));
  abc.moveToElement(Element).build().perform(); //concatenating the 2 methods because it will perform the action i.e.,move actions
	//to maintain capital letters
  WebElement xyz= driver.findElement(By.xpath(".//*[@id='twotabsearchtextbox']"));
  abc.keyDown(Keys.SHIFT).moveToElement(xyz).sendKeys("smallletters").build().perform();
  abc.contextClick(xyz).build().perform();
	}

}
