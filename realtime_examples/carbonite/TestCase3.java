package carbonite;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestCase3 {
 
	  WebDriver driver=new FirefoxDriver();
	  @BeforeTest
	  public void precondition(){
	  driver.get("https://www.carbonite.com/");
	  }
  
@Test
public void signin(){
	driver.findElement(By.xpath(".//*[@id='home-nav-signin']")).click();
	driver.findElement(By.xpath(".//*[@id='logonButton']")).click();
	// Verify error message in Email field
	String actualemailerror=driver.findElement(By.id("UserName_validationMessage")).getText();
	String expect="Email is required.";
	
	if (actualemailerror.equals(expect))
	{
		System.out.println("Error message for email is shown");
	}
	else
	{
		System.out.println("Error message for email not is shown");
	}
	// Verify error message in Password field
	String actualerror =driver.findElement(By.id("Password_validationMessage")).getText();
	String expecterror="Password is required.";

	if (actualerror.equals(expecterror))
	{
		System.out.println("Error message for password is shown");
	}
	else
	{
		System.out.println("Error message for password not is shown");
	}

}
@AfterClass
public void tearDown(){
    driver.close();
  
}
}

