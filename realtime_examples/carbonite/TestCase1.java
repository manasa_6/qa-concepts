/**
 * 
 */
/**
 * @author manasa
 *
 */
package carbonite;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestCase1 {
	
	  WebDriver driver=new FirefoxDriver();
	  @BeforeTest
	  public void precondition(){
	  driver.get("https://www.carbonite.com/");
	  }
	  // To verify free Trail Download option is available on home page
@Test
public void freetrail(){
	if(driver.getPageSource().contains("Download Free Trial"))
	{
		System.out.println("Free trail Download is available");		
	}
	else
	{
		System.out.println("Free trail Download is not available");
	}
}
// to verify that Email field is enabled and Defaulted to blank  
@Test
  public void email() { 
System.out.println( "Email field is Enabled::" + driver.findElement(By.xpath(".//*[@id='email']")).isEnabled());
WebElement emailbox=driver.findElement(By.xpath(".//*[@id='email']"));
	String Textemailbox=emailbox.getAttribute("value");
	System.out.println("Text in the email is blank::" +Textemailbox.isEmpty());

  }
	// to verify that Confirm Email field is enabled and Defaulted to blank  
  @Test
  public void confirmEmail(){
	
	  System.out.println( "confirm emial field is Enabled::" + driver.findElement(By.id("confirmEmail")).isEnabled());
	  WebElement cnfrmemailbox=driver.findElement(By.xpath(".//*[@id='confirmEmail']"));
		String Textemailbox=cnfrmemailbox.getAttribute("value");
		System.out.println("Text in the Confirm Email is blank::" +Textemailbox.isEmpty());
  }
	// to verify that Password field is enabled and Defaulted to blank  
  @Test
  public void password()
  {
System.out.println( "Password field is Enabled::" + driver.findElement(By.id("trialFormBody")).isEnabled());
	  WebElement passwordbox=driver.findElement(By.xpath(".//*[@id='trialFormBody']/div/div/div[2]/div/div[2]/form/div[3]/strong-password/ng-include/div/input"));
			String Textpassword=passwordbox.getAttribute("value");
			System.out.println("Text in the password field is blank::" +Textpassword.isEmpty());  
  }
	// to verify that confirm password field is enabled and Defaulted to blank  
  
  @Test
  public void cnfrmpassword()
  {
	  System.out.println( "confirm Password field is Enabled::" + driver.findElement(By.xpath(".//*[@id='confirmPassword']")).isEnabled());
	  WebElement confirmfield=driver.findElement(By.xpath(".//*[@id='confirmPassword']"));
			String confirmpassword=confirmfield.getAttribute("value");
			System.out.println("Text in the confirm password field is blank::" +confirmpassword.isEmpty());
  }
	// to verify that Trail Button field is enabled  
  @Test
  public void Trailbutton()
  {
	  System.out.println( "Free Trail button is Enabled::" + driver.findElement(By.id("btn-hero-form-try")).isEnabled());
	  WebElement downloadbtn=driver.findElement(By.xpath(".//*[@id='btn-hero-form-try']"));
			String download=downloadbtn.getAttribute("value");
			System.out.println("Text in the Trail button is blank::" +download.isEmpty());
  }
  @AfterClass
  public void tearDown(){
 driver.close();
    
  } }
