package carbonite;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestCase2 {

	  WebDriver driver=new FirefoxDriver();
	 
@BeforeTest
	  public void precondition(){
	  driver.get("https://www.carbonite.com/");
	  driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	  }

	// verify that menu options are present   
 @Test
 public void menuoptions(){
	 System.out.println( "Pricing option is Enabled::" + driver.findElement(By.id("home-nav-pricing")).isEnabled());
	 System.out.println( "Features option is Enabled::" + driver.findElement(By.xpath(".//*[@id='home-nav-features']")).isEnabled());
	 System.out.println( "cloud Backup101 option is Enabled::" + driver.findElement(By.id("home-nav-cloudbackup101")).isEnabled());
	 System.out.println( "Support option is Enabled::" + driver.findElement(By.xpath(".//*[@id='home-nav-support']")).isEnabled());
	  
  }

 @Test(priority=1)
 public void verifypricing(){
	 
	WebElement pricingpage=driver.findElement(By.id("home-nav-pricing"));
	pricingpage.click();
	if(driver.getPageSource().contains("BASIC"))
	  {
	    System.out.println("Correct Pricing page was diplayed ");
	  }
	else
	  {
	    System.out.println("Correct Pricing page was diplayed");
	  }
	 
 }
 @Test(priority=2)
 public void featuresverify(){
	WebElement featurepage=driver.findElement(By.id("home-nav-features"));
	featurepage.click();
	
	if(driver.getPageSource().contains("Backup that won't slow you down"))
	  {
	    System.out.println("Correct Feature page was diplayed ");
	  }
	else
	  {
	    System.out.println("Fail");
	  }
	 
 }
@Test(priority=3)
 public void cloudverify(){
	WebElement cloudpage=driver.findElement(By.id("home-nav-cloudbackup101"));
	cloudpage.click();
	
	if(driver.getPageSource().contains("Cloud Backup 101"))
	  {
	    System.out.println("Correct Cloud Backup page was diplayed ");
	  }
	else
	  {
	    System.out.println("Correct Cloud Backup page was not diplayed");
	  }
	 
 }
 @Test(priority=4)
 public void supportverify(){
	WebElement cloudpage=driver.findElement(By.id("home-nav-support"));
	cloudpage.click();
	
	if(driver.getPageSource().contains("Your Personal Assistant"))
	  {
	    System.out.println("Correct Support page was displayed ");
	  }
	else
	  {
	    System.out.println("Correct Support page was not displayed");
	  }
	 
 }
 @AfterClass
 public void tearDown(){
     driver.close();
   
 }
 

}


