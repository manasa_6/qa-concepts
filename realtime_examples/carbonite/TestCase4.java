package carbonite;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestCase4 {
	 WebDriver driver=new FirefoxDriver();
	  @BeforeTest
	  public void precondition(){
	  driver.get("https://www.carbonite.com/");
	  }
	  // To verify reCathcha error message
	  @Test
	  public void reCapthcha(){
	  	driver.findElement(By.xpath(".//*[@id='home-nav-signin']")).click();
	  	driver.findElement(By.xpath(".//*[@id='UserName']")).sendKeys("teja@gmail.com");
	  	driver.findElement(By.xpath(".//*[@id='Password']")).sendKeys("teja");
	  	driver.findElement(By.xpath(".//*[@id='logonButton']")).click();
		String actualmsg =driver.findElement(By.xpath(".//*[@id='LogOn']/div/ul/li")).getText();
		String expectedmsg="Please complete the reCAPTCHA challenge below.";

		if (actualmsg.equals(expectedmsg))
		{
			System.out.println("Error message for reCapthcha is shown");
		}
		else
		{
			System.out.println("Error message for reCapthcha is not shown");
		}
	  	
}
	  @AfterClass
	  public void tearDown(){
	      driver.close();
	    
	  }
}
